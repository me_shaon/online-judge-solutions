#include <cstdio>
#include <iostream>
using namespace std;

int main() {
	freopen("sitin.txt", "r", stdin);
	freopen("sitout.txt", "w", stdout);

	int row, seat, guest, sitting, standing;

	cin>>row>>seat>>guest;

	int total_seat = seat * row;

    if(guest >= total_seat)
    {
        sitting = total_seat;
        standing = guest - total_seat;
    }
    else
    {
        standing = 0;
        sitting = guest;
    }

    cout<<sitting<<" "<<standing<<endl;

	return 0;
}
