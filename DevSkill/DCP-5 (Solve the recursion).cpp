/*
Problem link- https://www.devskill.com/CodingProblems/ViewProblem/5
*/
#include<iostream>
#include<algorithm>

using namespace std;

int main()
{
    int a[5];

    while(cin>>a[0]>>a[1]>>a[2])
    {
        sort(a, a+3);

        cout<<(2 * a[1] + a[0])<<endl;
    }

    return 0;
}
