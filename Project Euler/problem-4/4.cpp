#include<stdio.h>
#include<algorithm>
using namespace std;

bool is_palindrome(int num)
{
	int rev = num;
	int tmp=0;
	while(rev != 0)
	{
		tmp = (tmp*10)+(rev%10);
		rev/=10;
	}

	return (num == tmp);
}


int main()
{
	bool flag = true;
	int max_pal = 0;
	for(int i=999;i>=1;i--)
	{
		for(int j=999;j>=1;j--)
		{
			int num = i*j;
			if(is_palindrome(num))	
			{
				max_pal = max(max_pal, num);
			}
		}
	}

	printf("%d\n", max_pal);

	return 0;
}
