#include<iostream>

using namespace std;

#define LL long long int 

LL get_series_sum(LL n)
{
    return (LL)((n*(n+1))/2);
}

int main(){
	int t;   

	int num;

	num = (1000-1);

	LL sum = 0;

	LL three = 3 * get_series_sum(num/3);
	LL five = 5 * get_series_sum(num/5);
	LL fifteen = 15 * get_series_sum(num/15);
	sum = three + five - fifteen;

	cout<<sum<<endl;

    return 0;
}
